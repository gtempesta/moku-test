# Moku-test #

Test con utilizzo di html5, css3 e AngularJS.   

Dopo aver clonato il progetto, esegui i seguenti comandi:  

`cd moku-test` per posizionarti sulla cartella del progetto.  

`npm install` per installare tutti i pacchetti.  

`npm start` per installare i pacchetti di bower, avviare un server locale e visualizzare la app. 

